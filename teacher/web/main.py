import psycopg2
import json
from flask import Flask
from functools import wraps
from flask import request,Response
app = Flask(__name__)

f = open("database_info",'r')
database_info = []
for line in f:
    database_info.append(line.strip())
f.close()
f = open("http_info",'r')
http_info = []
for line in f:
    http_info.append(line.strip())
f.close()


### sql adapter
def select (table,variables=None,filter=None): #return all selected content in 2d tuple
    conn = psycopg2.connect(database_info[0])
    cur = conn.cursor()
    command = "SELECT "
    if variables != None:
        if type(variables) == type([]):
            for i in range(len(variables)):
                command += str(variables[i])
                if i != len(variables)-1:
                    command += ","
                else:
                    command += " "
        else:
            command += str(variables) + " "
    else:
        command += "* "
    command += "FROM "
    if filter:
        command += table+" WHERE "+str(filter)
    else:
        command += table
    cur.execute(command)
    result = cur.fetchall()
    conn.commit()
    cur.close()
    conn.close()
    print 'select command executed: ' + command
    return result

def insert_row (table,values,sequence=None):
    inserted_values = ' VALUES '
    if type(values[0]) != type([]):
        #check length of input
        if sequence != None and len(values) != len(sequence):
            print "inserted values have different length with sequence"
            raise ValueError
        ###
        inserted_values += '('
        for i in range(len(values)):
            if type(values[i]) == type(''):
                inserted_values += '\'' + values[i] + '\''
            else:
                inserted_values += str(values[i])
            if i != len(values)-1:
                inserted_values += ','
        inserted_values += ')'
    else:
        #check length of input
        for i in range(len(values)-1):
            if len(values[i]) != len(values[i+1]):
                print 'inserted values have different length'
                raise ValueError
        if sequence != None and len(values[0]) != len(sequence):
            print "inserted values have different length with sequence"
            raise ValueError
        ###
        for j in range(len(values)):
            inserted_values += '('
            for i in range(len(values[j])):
                if type(values[j][i]) == type(''):
                    inserted_values += '\'' + values[j][i] + '\''
                else:
                    inserted_values += str(values[j][i])
                if i != len(values[j]) - 1:
                    inserted_values += ','
            inserted_values += ')'
            if j != len(values)-1:
                inserted_values += ','
    sequence_variables = ''
    if sequence != None:
        sequence_variables += " ("
        for i in range(len(sequence)):
            sequence_variables += str(sequence[i])
            if i != len(sequence)-1:
                sequence_variables += ','
        sequence_variables += ')'
    command = 'INSERT INTO '+ table + sequence_variables + inserted_values
    conn = psycopg2.connect(database_info[0])
    cur = conn.cursor()
    cur.execute(command)
    conn.commit()
    cur.close()
    conn.close()
    print 'insert command executed: ' + command
    return

def delete_row(table,filter): # set filter to None to delete all from the table
    command = 'DELETE FROM '+ table
    if filter != None:
        command += ' WHERE ' + str(filter)
    conn = psycopg2.connect(database_info[0])
    cur = conn.cursor()
    cur.execute(command)
    conn.commit()
    cur.close()
    conn.close()
    print "command executed: " + command
    return

def update(table,variables,filter):
    command = "UPDATE "+ table + " SET " + variables + " WHERE " + filter
    conn = psycopg2.connect(database_info[0])
    cur = conn.cursor()
    cur.execute(command)
    conn.commit()
    cur.close()
    conn.close()
    print "command executed: " + command
    return

def column_name(table):
    command = "SELECT column_name FROM information_schema.columns WHERE table_name=\'"+table+"\'"
    conn = psycopg2.connect(database_info[0])
    cur = conn.cursor()
    cur.execute(command)
    conn.commit()
    result = cur.fetchall()
    cur.close()
    conn.close()
    print "command executed "+ command
    return result

### http adapter
###   Authentication
def check_auth(username, password):
    """This function is called to check if a username /
    password combination is valid.
    """
    return username == http_info[0] and password == http_info[1]

def authenticate():
    """Sends a 401 response that enables basic auth"""
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated



@app.route('/',methods = ["GET","POST"])
@requires_auth
def secret_page():
    if request.method == "POST":
        # print "request: " + str(request.form)
        type = request.form["type"]
        if type == "select":
            table = request.form["table"]
            filter = None
            variables = None
            if "variables" in request.form:
                variables = request.form["variables"].split(",")
            if "filter" in  request.form:
                filter = request.form["filter"]
            return str(select(table,variables,filter))
        if type == "insert_row":
            table = request.form["table"]
            values = request.form["values"].split(",")
            sequence = None
            if "sequence" in  request.form:
                sequence = request.form["sequence"].split(",")
            insert_row(table,values,sequence)
            return "insert successful"
        if type == "delete_row":
            table = request.form["table"]
            filter = request.form["filter"]
            try:
                delete_row(table,filter)
                return "delete row successful"
            except:
                print "row not found"
                return "delete row failed"
        if type == "update":
            table = request.form["table"]
            variables = request.form["variables"]
            filter = request.form["filter"]
            update(table,variables,filter)
            return "update successful"
        if type == "column_name":
            table = request.form["table"]
            return str(column_name(table))
    return



if __name__ == "__main__":
    app.run()


