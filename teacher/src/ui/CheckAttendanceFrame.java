/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author Administrator
 */
public class CheckAttendanceFrame extends javax.swing.JFrame {
    
    MainFrame mainFrame;
    CheckAttendanceFrame mFrame;
    String lectureName;
    String[] lectureInfo;
    /**
     * Creates new form CheckAttendance
     */
    public CheckAttendanceFrame(MainFrame mainFrame1,String lectureName1, String[] lectureInfo1) {
        initComponents();
        mainFrame = mainFrame1;
        mFrame = this;
        lectureName = lectureName1;
        lectureInfo = lectureInfo1;
        mFrame.setLocationRelativeTo(mainFrame);
        mFrame.setVisible(true);
//        mFrame.setEnabled(false);
        new FetchAttendanceThread().start(); 
        jLabelSubject.setText(lectureInfo[1]);
        jLabelCohort.setText(lectureInfo[0]);
        jLabelVenue.setText(lectureInfo[2]);
        jLabelDate.setText(lectureInfo[4]);
        jLabelTime.setText(lectureInfo[3]);
    }
    
    class FetchAttendanceThread extends Thread{
        @Override
        public void run(){
            WindowListener exitWindow = new WindowAdapter(){
                @Override
                public void windowClosing(WindowEvent e){
//                    mainFrame.setEnabled(true);
//                    mainFrame.setFocusable(true);
//                    mainFrame.setFocusableWindowState(true);
                    mFrame.dispose();
                }
            };
            mFrame.addWindowListener(exitWindow);
            mFrame.setTitle("Fetching attendance data...");
            
            try {
                String[][] data = new method.Http().connection("type=select&table="+mainFrame.subject.toLowerCase()+"&filter=id=\'"+lectureName+"\'");
                String[][] columnName = new method.Http().connection("type=column_name&table="+mainFrame.subject.toLowerCase());
                if (data == null){
                    JOptionPane.showMessageDialog(mFrame, "Lecture not found.", "Error", JOptionPane.ERROR_MESSAGE);
                    mFrame.dispose();
                }
                String[][] content = new String[data[0].length-2][2];
                for (int i=2;i<data[0].length;i++){
//                    System.out.println(i);
//                    System.out.println(i-2);
//                    System.out.println(content.length);
//                    System.out.println(columnName.length);
                    content[i-2][0] = columnName[i][0];
                    if (data[0][i].equals("None")){
                        content[i-2][1] = "false";
                    }else{
                    content[i-2][1] = data[0][i];}
                }
//                FTPClient ftp = new FTPClient();
//                ftp.connect("niqiukun.web.fc2.com");
//                ftp.login("niqiukun", "Zaq12wsX");
//                InputStream ifs1 = ftp.retrieveFileStream("/smartattendance/lectures/" + lectureName);
//                String d = IOUtils.toString(ifs1,"UTF-8");
//                ifs1.close();
//                ftp.logout();
//                ftp.disconnect();
//                ftp.connect("niqiukun.web.fc2.com");
//                ftp.login("niqiukun", "Zaq12wsX");
//                InputStream ifs2 = ftp.retrieveFileStream("/smartattendance/student_data");
//                String m = IOUtils.toString(ifs2,"UTF-8");
//                ifs2.close();
//                ftp.logout();
//                ftp.disconnect();
//                String [] student_data_temp = m.split("\n");
//                String [][] student_data = new String[student_data_temp.length][2];
//                for (int i=0; i<student_data_temp.length;i++){
//                    if (student_data_temp[i].charAt(student_data_temp[i].length()-1) == '\r'){
//                        student_data_temp[i] = student_data_temp[i].substring(0,student_data_temp[i].length()-1);
//                    }
//                    student_data[i][0] = student_data_temp[i].split(",")[0];
//                    student_data[i][1] = student_data_temp[i].split(",")[2];
//                }
//                String [] l = d.split("\n");
//                String[][] content = new String [l.length-1][3];
//                for (int i=0; i<l.length;i++){
//                    if (l[i].charAt(l[i].length()-1) == '\r'){
//                        l[i] = l[i].substring(0,l[i].length()-1);
//                    }
//                    if (i>0){
//                        content[i-1][1] = l[i].split(" ")[0];
//                        if (l[i].split(" ").length == 2){
//                            content[i-1][2] = l[i].split(" ")[1];
//                        }else{
//                            content[i-1][2] = "false";
//                        }
//                        boolean found = false;
//                        for (int j=0;j<student_data.length;j++){
//                            if(content[i-1][1].equals(student_data[j][0])){
//                                found = true;
//                                content[i-1][0] = student_data[j][1];
//                                break;
//                            }
//                        }
//                        if (found == false){content[i-1][0] = "not found";}
//                    }
//                }
                String[] cols = {"Registration Number", "Attendance"};
                DefaultTableModel model = new DefaultTableModel(null, cols) {
                    boolean[] canEdit = new boolean [] {
                        false, false
                    };
                    public boolean isCellEditable(int rowIndex, int columnIndex) {
                        return canEdit [columnIndex];
                    }
                    @Override
                    public Class<?> getColumnClass(int column) {
                        if (column == 1) {
                            return Boolean.class;
                        } else {
                            return String.class;
                        }
                    }
                };
//                model = (DefaultTableModel) jTable1.getModel();
                for (int i = 0;i < content.length;i++){
                    model.addRow(content[i]);
                    if (model.getValueAt(i,1).equals("true")){
                        model.setValueAt(true, i, 1);
                    }
                    else{
                        model.setValueAt(false, i, 1);
                    }
                }
                jTable1.setModel(model);
                mFrame.setEnabled(true);
                mFrame.setTitle("Check Attendance");
            } catch (IOException ex) {
                System.err.println(ex.getMessage());
                JOptionPane.showMessageDialog(mFrame, "Network error.", "Error", JOptionPane.ERROR_MESSAGE);
                mFrame.setEnabled(true);
                mFrame.setTitle("Check Attendance");
            }
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabelSubject = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabelDate = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabelTime = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel7 = new javax.swing.JLabel();
        jLabelVenue = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabelCohort = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Check Attendance");

        jLabel1.setText("Subject:");

        jLabelSubject.setText("jLabel2");

        jLabel3.setText("Date:");

        jLabelDate.setText("jLabel4");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("Time:");

        jLabelTime.setText("jLabel6");

        jTable1.setFont(new java.awt.Font("Lucida Grande", 0, 13)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Registration Number", "Attendance"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable1.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(jTable1);

        jLabel7.setText("Venue:");

        jLabelVenue.setText("jLabel8");

        jLabel2.setText("Cohort:");

        jLabelCohort.setText("jLabel4");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelDate, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelTime, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelSubject, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelCohort, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabelVenue, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 73, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabelSubject))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabelCohort)
                    .addComponent(jLabel7)
                    .addComponent(jLabelVenue))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabelDate)
                    .addComponent(jLabel5)
                    .addComponent(jLabelTime))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 316, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CheckAttendanceFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CheckAttendanceFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CheckAttendanceFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CheckAttendanceFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                
                
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabelCohort;
    private javax.swing.JLabel jLabelDate;
    private javax.swing.JLabel jLabelSubject;
    private javax.swing.JLabel jLabelTime;
    private javax.swing.JLabel jLabelVenue;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
