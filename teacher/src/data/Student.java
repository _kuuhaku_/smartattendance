/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author Administrator
 */
public class Student {
    private String rawData;
    private String nric;
    private String name;
    private String attendance;
    private Student(String data){
        rawData = data;
        String[] temp = rawData.split(",");
        nric = temp[0];
        name = temp[1];
        attendance = temp[2];
    }
    private void setAttendance(boolean b){
        if (b){
            attendance = "Present";
        }else{
            attendance = "Absent";
        }
    }
    @Override
    public String toString(){
        String temp = "";
        temp += nric+","+name+","+attendance;
        return temp;
    }
}
