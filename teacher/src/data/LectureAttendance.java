/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.util.Arrays;

/**
 *
 * @author Administrator
 */
public class LectureAttendance {
    private String lectureRawData;
    private String[] lectureData;
    private String lectureInfoData;
    private String[] attendance;
    String subject;
    String cohort;
    String venue;
    String date;
    String time;
    public LectureAttendance(String data){
        lectureRawData = data;
        lectureData = lectureRawData.split("\n");
        lectureInfoData = lectureData[0];
        if (lectureData.length > 1){
            attendance = Arrays.copyOfRange(lectureData, 1, lectureData.length);
        }else{
            attendance = null;
        }
        String[] lectureInfo = lectureInfoData.split("_");
        cohort = lectureInfo[0];
        subject = lectureInfo[1];
        venue = lectureInfo[2];
        time = lectureInfo[3];
        date = lectureInfo[4];
    }
    
}
