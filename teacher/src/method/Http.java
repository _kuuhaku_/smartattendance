/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package method;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import java.util.List;
import org.apache.commons.io.IOUtils;
import java.lang.String;
import java.util.Base64;

/**
 *
 * @author Administrator
 */
public class Http {
    public String[][] connection(String str) throws IOException{ //"type=select&table=students&variables=id,password"
        HttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://127.0.0.1:5000/");
        String authStr = "username:password";
        String encoding = Base64.getEncoder().encodeToString(authStr.getBytes());
        httpPost.setHeader("Authorization", "Basic " + encoding);



        // Request parameters and other properties.
        String[] strArray = str.split("&");
        String index,content;
        List<NameValuePair> params = new ArrayList<>(2);
        for (int i=0;i<strArray.length;i++){
            index = strArray[i].split("=",2)[0];
            content = strArray[i].split("=",2)[1];
            params.add(new BasicNameValuePair(index,content));
        }
//        params.add(new BasicNameValuePair("type", "select"));
//        params.add(new BasicNameValuePair("table", "students"));
        httpPost.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));

        //Execute and get the response.
        HttpResponse response = httpClient.execute(httpPost);
        HttpEntity entity = response.getEntity();

        if (entity != null) {
            InputStream inStream = entity.getContent();
            try {
                String result = IOUtils.toString(inStream, "UTF-8");
                if (str.contains("type=select")||str.contains("type=column_name")){
                    return parseResult(result);
                }
                else {
                    System.out.println("result: "+ result);
                }
                return null;
                // do something useful
            } finally {
                inStream.close();
            }        
        }
        return null;
    }
    
    public String[][] parseResult(String result){
        System.out.println("result: "+result);
        try{
            if (result.equals("[]") || result == null){
                return null;
            }
            result = result.substring(1, result.length());
            List<String[]> parsedResult = new ArrayList<>(2);
            int start = 0;
            for (int i=0;i<result.length();i++){
                if (result.charAt(i) == '('){
                    start = i;
                }else if (result.charAt(i) == ')'){
                    String temp = result.substring(start + 1,i);
                    String[] tempArray = temp.split(",");
                    for (int j=0;j<tempArray.length;j++){
                        if (tempArray[j].contains(" ")){
                            tempArray[j] = tempArray[j].substring(1,tempArray[j].length());
                        }
                        if (tempArray[j].contains("\'")){
                            tempArray[j] = tempArray[j].substring(1,tempArray[j].length()-1);
                        }
                    }
                    parsedResult.add(tempArray);
                }
            }
            String[][] parsedResultArray = new String[parsedResult.size()][parsedResult.get(0).length];
            for (int i=0;i<parsedResult.size();i++){
                for(int j=0;j<parsedResult.get(i).length;j++){
                    parsedResultArray[i][j] = parsedResult.get(i)[j];
                    System.out.println("parsed result:" + parsedResultArray[i][j]);
                }
            }
            return parsedResultArray;
        }catch (Exception e){
        return null;}
    }
}


