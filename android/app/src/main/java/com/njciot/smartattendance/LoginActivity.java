package com.njciot.smartattendance;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.*;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import org.apache.commons.net.ftp.*;
import org.apache.commons.io.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class LoginActivity extends AppCompatActivity {

    String scanResult = null;
    String imei = null;
    ProgressDialog pd;
    LoginActivity mActivity = this;
    String[] entry;
    String[][] userInfo;
    int user = -1;
    FTPClient ftp = new FTPClient();
    String server = "niqiukun.web.fc2.com";
    String updatedInfo = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.CAMERA}, 1);
    }

    public void startScanning(View v){
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(BarcodeActivity.class);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
        integrator.setPrompt("Please scan your NRIC/FIN barcode");
        integrator.setOrientationLocked(true);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    public void startLogin(View v){
        EditText ed = (EditText) findViewById(R.id.editText);
        login(ed.getText().toString());
    }

    private class UpdateImeiTask extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {
                ftp.connect(server);
                ftp.login("niqiukun", "Zaq12wsX");
                OutputStream out = ftp.storeFileStream("/smartattendance/student_data");
                out.write(updatedInfo.getBytes());
                out.close();
                ftp.logout();
                ftp.disconnect();
                pd.dismiss();
                return null;
            } catch (IOException e) {
                pd.dismiss();
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(mActivity);
                dlgAlert.setTitle("Error");
                dlgAlert.setMessage("Network error. Make sure that your phone has a stable Internet connection.");
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.create().show();
                return null;
            }
        }
        protected void onPostExecute(String data) {
            EditText ed = (EditText) findViewById(R.id.editText);
            login(ed.getText().toString());
        }
    }

    private class RetrieveInfoTask extends AsyncTask<Void, Void, String> {
        private Exception e;
        protected String doInBackground(Void... params){
            try {
                ftp.connect(server);
                ftp.login("niqiukun", "Zaq12wsX");
                InputStream in = ftp.retrieveFileStream("/smartattendance/student_data");
                String data = IOUtils.toString(in, "UTF-8");
                in.close();
                ftp.logout();
                ftp.disconnect();
                return data;
            } catch (IOException e) {
                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(mActivity);
                dlgAlert.setTitle("Error");
                dlgAlert.setMessage("Network error. Make sure that your phone has a stable Internet connection.");
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.create().show();
                return null;
            }
        }
        protected void onPostExecute(String data) {
            if(data == null){
                return;
            }
            entry = data.split("\n");
            userInfo = new String[entry.length][];
            for (int i = 0; i < entry.length; i++) {
                userInfo[i] = entry[i].split(",");
            }
            for (int i = 0; i < userInfo.length; i++) {
                if (userInfo[i][0].equals(scanResult)) {
                    user = i;
                }
            }
            pd.dismiss();
            imei = "123456789012345";
            if(user != -1){
                if(userInfo[user][1].equals("null")){
                    AlertDialog.Builder dlgAlert = new AlertDialog.Builder(mActivity);
                    dlgAlert.setTitle("Alert");
                    dlgAlert.setMessage("User profile not registered. Bind this phone with your user profile?");
                    dlgAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            userInfo[user][1] = imei;
                            for (int i = 0; i < userInfo.length; i++) {
                                for (int j = 0; j < userInfo[i].length; j++) {
                                    updatedInfo += userInfo[i][j];
                                    if (j == userInfo[i].length - 1 && i < userInfo.length - 1){
                                        updatedInfo += "\n";
                                    }else if(!(j == userInfo[i].length - 1 && i == userInfo.length - 1)){
                                        updatedInfo += ",";
                                    }
                                }
                            }
                            new UpdateImeiTask().execute();
                            pd = ProgressDialog.show(LoginActivity.this, null, "Updating user profile...");
                        }
                    });
                    dlgAlert.setNegativeButton("No", null);
                    dlgAlert.create().show();
                }
                else if(userInfo[user][1].equals(imei)){
                    Toast.makeText(mActivity, "Logged in", Toast.LENGTH_LONG).show();
                    try {
                        File f = new File(getFilesDir() + "/login_status.dat");
                        f.createNewFile();
                        OutputStream fos = new FileOutputStream(getFilesDir() + "/login_status.dat");
                        fos.write(("first_time_login=false"+"\n").getBytes());
                        fos.write(("NRIC="+ scanResult+"\n").getBytes());
                        fos.write(("imei="+ imei+"\n").getBytes());
                        fos.write(("name=" + userInfo[user][2] + "\n").getBytes());
                        fos.write(("offline_att=false").getBytes());
                        fos.close();
                        startActivity(new Intent(mActivity, MainActivity.class));
                        mActivity.finish();
//                        System.out.println("WRITE SUCCESS");
                    } catch (IOException e1) {
//                        System.out.println("WRITE FAILED");
                    }
                }else{
                    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(mActivity);
                    dlgAlert.setTitle("Error");
                    dlgAlert.setMessage("User profile does not match. If you have changed your phone, please contact system administrator to reset your user profile.");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.create().show();
                }
            }else{
                AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(mActivity);
                dlgAlert.setTitle("Error");
                dlgAlert.setMessage("Invalid user profile. Please ensure that you have input the correct username and scanned your NRIC/FIN barcode correctly.");
                dlgAlert.setPositiveButton("OK", null);
                dlgAlert.create().show();
            }
        }
    }

    public void login(final String username){
        pd = ProgressDialog.show(LoginActivity.this, null, "Logging in...");
        if(!checkFIN(scanResult) || !(username.equals(scanResult))){
            pd.dismiss();
            AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
            dlgAlert.setTitle("Error");
            dlgAlert.setMessage("Invalid user profile. Please ensure that you have input the correct username and scanned your NRIC/FIN barcode correctly.");
            dlgAlert.setPositiveButton("OK", null);
            dlgAlert.create().show();
            return;
        }
        pd.dismiss();
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setTitle("Alert");
        dlgAlert.setMessage("The app is currently for demo purpose only. Please note that the connection with the server and upload of any data may be insecure. Do not use real personal information in any case.");
        dlgAlert.setPositiveButton("Continue", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                pd = ProgressDialog.show(LoginActivity.this, null, "Logging in...");
                new RetrieveInfoTask().execute();
            }
        });
        dlgAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dlgAlert.create().show();
    }

    private boolean checkFIN(String fin){
        if(fin.length() != 9){
            return false;
        }
        char prefix = fin.charAt(0);
        char suffix = fin.charAt(fin.length() - 1);
        boolean flag = false;
        char[] validInitial = {'S', 'T', 'F', 'G'};
        for (char aValidInitial : validInitial) {
            if (prefix == aValidInitial) {
                flag = true;
            }
        }
        if(!flag){
            return false;
        }
        String num = fin.substring(1, fin.length() - 1);
        try{
            Integer.parseInt(num);
        }catch (NumberFormatException e){
            return false;
        }
        int[] weight = {2, 7, 6, 5, 4, 3, 2};
        int sum = 0;
        for (int i = 0; i < num.length(); i++) {
            sum += Character.getNumericValue(num.charAt(i)) * weight[i];
        }
        int resultInt = sum % 11;
        char[] charTableS = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'Z', 'J'};
        char[] charTableT = {'H', 'I', 'Z', 'J', 'A', 'B', 'C', 'D', 'E', 'F', 'G'};
        char[] charTableF = {'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'T', 'U', 'W', 'X'};
        char[] charTableG = {'T', 'U', 'W', 'X', 'K', 'L', 'M', 'N', 'P', 'Q', 'R'};
        char resultChar = ' ';
        switch (prefix){
            case 'S':
                resultChar = charTableS[10 - resultInt];
                break;
            case 'T':
                resultChar = charTableT[10 - resultInt];
                break;
            case 'F':
                resultChar = charTableF[10 - resultInt];
                break;
            case 'G':
                resultChar = charTableG[10 - resultInt];
                break;
        }
        return suffix == resultChar;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() != null) {
                scanResult = result.getContents();
                int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
                } else {
                    EditText ed = (EditText) findViewById(R.id.editText);
                    EditText edp = (EditText) findViewById(R.id.editText2);
                    Button b = (Button) findViewById(R.id.button);
                    imei = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
                    edp.setText(imei);
                    b.setEnabled(true);
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if ((grantResults.length > 1) && (grantResults[0] == PackageManager.PERMISSION_GRANTED) && (grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    System.out.println("PERMISSION GRANTED");
                }else{
                    System.out.println("PERMISSION DENIED");
                    Toast.makeText(this, "Permissions are necessary for the app to run correctly. You may change the permissions at Settings >> Apps >> Smart Attendance >> Permission", Toast.LENGTH_LONG).show();
                    ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.CAMERA}, 1);
                }
                break;
            default:
                break;
        }
    }
}
