package com.njciot.smartattendance;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.io.File;

public class LauncherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        File f = new File(getFilesDir() + "/login_status.dat");
        if(f.exists()){
            startActivity(new Intent(this, MainActivity.class));
//            InputStream fis = null;
//            try {
//                fis = new FileInputStream(getFilesDir() + "/login_status.dat");
//                String data1 = IOUtils.toString(fis, "UTF-8");
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }else{
            startActivity(new Intent(this, LoginActivity.class));
        }
        this.finish();
    }
}
