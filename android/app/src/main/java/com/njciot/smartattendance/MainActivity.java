package com.njciot.smartattendance;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.*;
import org.apache.commons.net.ntp.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Array;
import java.util.Arrays;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    String nric = null;
    String name = null;
    FTPClient ftp = new FTPClient();
    ProgressDialog pd;
    String scanResult;
    Long correctedTime;
    MainActivity mActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("OnCreate method triggered");
        setContentView(R.layout.activity_main);
        mActivity = this;
        boolean offline_att;
        try {
            FileInputStream fis = new FileInputStream(getFilesDir() + "/login_status.dat");
            String data1 = IOUtils.toString(fis, "UTF-8");
            String[] entry = data1.split("\n");
            for (String anEntry : entry) {
                if (anEntry.contains("NRIC=")) {
                    nric = anEntry.substring(anEntry.indexOf('=') + 1);
                }
                if (anEntry.contains("name=")) {
                    name = anEntry.substring(anEntry.indexOf('=') + 1);
                }
                if (anEntry.contains("offline_att=")){
                    String tmp = anEntry.substring(anEntry.indexOf('=') + 1);
                    offline_att = tmp.equals("true");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        TextView nricTextView = (TextView) findViewById(R.id.textView9);
        nricTextView.setText(nric);
        TextView nameTextView = (TextView) findViewById(R.id.nricTextView);
        nameTextView.setText(name);
    }

    public void startQR(View v){
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(BarcodeActivity.class);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setPrompt("Please scan the QR code");
        integrator.setOrientationLocked(true);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();
    }

    private boolean isInternetAvailable(){
        try {
            InetAddress ipAddr = InetAddress.getByName("www.google.com");
            System.out.println("internet available");
            return !ipAddr.equals("");
        } catch (Exception e) {
            System.out.println("internet not available");
            return false;
        }
    }

    private long getInternetTime(){
        try {
            System.out.println("getting internet time...");
            String TIME_SERVER = "time-a.nist.gov";
            NTPUDPClient timeClient = new NTPUDPClient();
            InetAddress inetAddress = null;
            inetAddress = InetAddress.getByName(TIME_SERVER);
            TimeInfo timeInfo = timeClient.getTime(inetAddress);
            System.out.println("get internet time successful");
            return timeInfo.getMessage().getTransmitTimeStamp().getTime();
        } catch (IOException e){
            System.out.println("fail to get internet time");
            e.printStackTrace();
        }
        return 0;
    }

    private boolean isInTime(String fileName, long time){
        try {
            ftp.connect("niqiukun.web.fc2.com");
            ftp.login("niqiukun", "Zaq12wsX");
            InputStream fis = ftp.retrieveFileStream("/smartattendance/lectures/" + fileName);
            String[] content = IOUtils.toString(fis, "UTF-8").split("\n");
            fis.close();
            ftp.logout();
            ftp.disconnect();
            System.out.println(Arrays.toString(content));
            System.out.println("current time: " + Long.toString(time));
            boolean inTime = false;
            String[] serverTimeStr = content[0].split(",");
            for (int i = 0; i < serverTimeStr.length; i++) {
                long serverTime = Long.parseLong(serverTimeStr[i]) * 1000;
                if(time - serverTime <= 30000) {
                    inTime = true;
                }
            }
            return inTime;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pd.dismiss();
                    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(mActivity);
                    dlgAlert.setTitle("Error");
                    dlgAlert.setMessage("Internal error occurred. Submission failed.");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.create().show();
                }
            });
            return false;
        }
        return false;
    }

    private class SubmitAttendanceTask extends AsyncTask<Void, Void, String> {
        protected String doInBackground(Void... params) {
            try {
                ftp.connect("niqiukun.web.fc2.com");
                ftp.login("niqiukun", "Zaq12wsX");
                FTPFile[] f = ftp.listFiles("/smartattendance/lectures");
                ftp.logout();
                ftp.disconnect();
                f = Arrays.copyOfRange(f, 2, f.length);
//                System.out.println("hahaha");
                boolean findFile = false;
                for(FTPFile anF : f){
                    System.out.println( "file name & scan result: "+ anF.getName() + ";" + Integer.toString(Arrays.hashCode(anF.getName().getBytes())) + ";" + scanResult);
                    if(Integer.toString(Arrays.hashCode(anF.getName().getBytes())).equals(scanResult)){
                        findFile = true;
                        System.out.println("file name: " + anF.getName());
                        if(isInTime(anF.getName(), correctedTime)){
                            ftp.connect("niqiukun.web.fc2.com");
                            ftp.login("niqiukun", "Zaq12wsX");
                            InputStream fis = ftp.retrieveFileStream("/smartattendance/lectures/" + anF.getName());
                            String[] content = IOUtils.toString(fis, "UTF-8").split("\n");
                            String[] r = new String[content.length];
                            for(int i = 0; i < content.length; i++){
                                System.out.println(content[i] + nric);
                                if(content[i].contains(nric) && !content[i].contains("true")){
                                    r[i] = nric + " true";
                                }else{
                                    r[i] = content[i];
                                }
                            }
                            fis.close();
                            ftp.logout();
                            ftp.disconnect();
                            ftp.connect("niqiukun.web.fc2.com");
                            ftp.login("niqiukun", "Zaq12wsX");
                            OutputStream fos = ftp.storeFileStream("/smartattendance/lectures/" + anF.getName());
                            for(String line : r){
                                System.out.println(line);
                                fos.write(line.getBytes());
                                fos.write("\n".getBytes());
                            }
                            fos.close();
                            ftp.logout();
                            ftp.disconnect();
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    pd.dismiss();
                                    Toast.makeText(mActivity, "Submission successful", Toast.LENGTH_LONG).show();
                                }
                            });
                        }else{
//                            System.out.println("Late submission");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    pd.dismiss();
                                    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(mActivity);
                                    dlgAlert.setTitle("Error");
                                    dlgAlert.setMessage("Submission failed. (Late submission)");
                                    dlgAlert.setPositiveButton("OK", null);
                                    dlgAlert.create().show();
                                }
                            });
                        }
                        break;
                    }
                }
                if (!findFile){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                            AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(mActivity);
                            dlgAlert.setTitle("Error");
                            dlgAlert.setMessage("Invalid QR code.");
                            dlgAlert.setPositiveButton("OK", null);
                            dlgAlert.create().show();
                        }
                    });
                }
                return null;
            } catch (IOException e) {
                return null;
            }
        }
        protected void onPostExecute(String data) {
            System.out.println("submit finished");
        }
    }

    private void submitAttendance(){
        new SubmitAttendanceTask().execute();
    }

    private class ReSubmitTask extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... params) {
            File f = new File(getFilesDir() + "/offline_attendance.dat");
            if(!f.exists()){
                return null;
            }
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(getFilesDir() + "/offline_attendance.dat");
                String[] entry = IOUtils.toString(fis, "UTF-8").split("\n");
                for (String anEntry : entry) {
                    Long offlineTime = null;
                    if (anEntry.contains("scanResult=")) {
                        scanResult = anEntry.substring(anEntry.indexOf('=') + 1);
                    }
                    if (anEntry.contains("offlineTime=")) {
                        offlineTime = Long.parseLong(anEntry.substring(anEntry.indexOf('=') + 1));
                    }
                    Long currentTime = Calendar.getInstance().getTimeInMillis();
                    Long timeOffset = getInternetTime() - currentTime;
                    correctedTime = offlineTime + timeOffset;
//                    submitAttendance();
                    runOnUiThread(new Runnable() {
                        public void run()
                        {
                            submitAttendance();
                        }
                    });
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {

        }
    }

    public void reSubmitAttendance(View v){
        new ReSubmitTask().execute();
    }

    private class ActivityResultTask extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... params) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pd = ProgressDialog.show(mActivity, null, "Submitting attendance...");
                }
            });
            if(isInternetAvailable()){
                correctedTime = getInternetTime();
//                submitAttendance();
                runOnUiThread(new Runnable() {
                    public void run()
                    {
                        submitAttendance();
                    }
                });
            }else{
                try {
                    File f = new File(getFilesDir() + "/offline_attendance.dat");
                    f.createNewFile();
                    OutputStream fos = new FileOutputStream(getFilesDir() + "/offline_attendance.dat");
                    fos.write(("scanResult="+ scanResult + "\n").getBytes());
                    fos.write(("offlineTime=" + Long.toString(Calendar.getInstance().getTimeInMillis()) + "\n").getBytes());
                    fos.close();
                    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(mActivity);
                    dlgAlert.setTitle("Error");
                    dlgAlert.setMessage("No Internet connectivity. Please submit your attendance again with Internet connection.");
                    dlgAlert.setPositiveButton("OK", null);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pd.dismiss();
                        }
                    });
                    dlgAlert.create().show();
//                        System.out.println("WRITE SUCCESS");
                } catch (IOException e) {
//                        System.out.println("WRITE FAILED");
                }
            }
//                Toast.makeText(this, scanResult, Toast.LENGTH_LONG).show();
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() != null) {
                scanResult = result.getContents();
                new ActivityResultTask().execute();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}

//fos.write("first_time_login=false".getBytes());
//        fos.write(("NRIC="+ scanResult).getBytes());
//        fos.write(("imei="+ imei).getBytes());